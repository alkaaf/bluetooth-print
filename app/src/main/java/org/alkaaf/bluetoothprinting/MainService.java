package org.alkaaf.bluetoothprinting;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by dalbo on 3/13/2018.
 */

public class MainService extends Service {
    class SvcBinder extends Binder {
        MainService getService() {
            return MainService.this;
        }
    }

    interface Msg {
        void sendMessage(String s);
    }

    int counter = 0;
    boolean start = true;
    Msg msg = null;

    public void setMsg(Msg msg) {
        this.msg = msg;
    }

    public void startCounter() {
        start = true;
        counter = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (start) {
                    counter++;
                    if (msg != null) msg.sendMessage(counter + "");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void stopCounter() {
        start = false;
    }

    SvcBinder binder = new SvcBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "oncreate", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "Binding", Toast.LENGTH_SHORT).show();
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(this, "Unbinding", Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "OnStarcCommand", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Destory", Toast.LENGTH_SHORT).show();
    }

    public void doToast() {
        Toast.makeText(this, "Tost from service", Toast.LENGTH_SHORT).show();
    }
}
