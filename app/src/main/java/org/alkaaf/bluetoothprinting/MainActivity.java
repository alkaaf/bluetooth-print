package org.alkaaf.bluetoothprinting;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.alkaaf.btprint.ActivityUSB;
import org.alkaaf.btprint.BluetoothPrint;

public class MainActivity extends AppCompatActivity {
    BluetoothPrint bluetoothPrint;
    MainService mainService;

    Button start, stop, bind, toast;
    TextView textView;
    ImageView iv;
    ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MainService.SvcBinder binder = ((MainService.SvcBinder) service);
            mainService = binder.getService();
            mainService.setMsg(new MainService.Msg() {
                @Override
                public void sendMessage(final String s) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(s);
                        }
                    });

                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    Intent intent;
    private Button unbind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bPrint = (Button) findViewById(R.id.bPrint);
        bluetoothPrint = new BluetoothPrint(this);
        start = findViewById(R.id.bStart);
        stop = findViewById(R.id.bStop);
        bind = findViewById(R.id.bBind);
        unbind = findViewById(R.id.bUnBind);
        toast = findViewById(R.id.bToast);
        textView = findViewById(R.id.text);
        iv = findViewById(R.id.iv);

        intent = new Intent(this, MainService.class);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startService(intent);
//                Intent intentUsb = new Intent(MainActivity.this, ActivityUSB.class);
//                startActivity(intentUsb);
            }
        });
        bind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindService(intent, sc, BIND_AUTO_CREATE);
            }
        });
        unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unbindService(sc);
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                stopService(intent);
                stopService(intent);
            }
        });
        toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainService.doToast();
            }
        });

        bPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothPrint.Builder bp = new BluetoothPrint.Builder(BluetoothPrint.Size.WIDTH80);
                bp.addFrontEnd("Hello world", "end");
                BluetoothPrint.with(MainActivity.this)
                        .setData(bp.getByte())
                        .print();
            }
        });
        doTheDrawable();
    }

    public void doTheDrawable() {

        Bitmap qr = QRCode("HOHOHO", 300, 300);
        final Bitmap layer = Bitmap.createBitmap(900, 300, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(layer);

        Paint title = new Paint();
        title.setColor(Color.BLACK);
        title.setTextSize(50);
        title.setTypeface(Typeface.DEFAULT_BOLD);
        title.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        Paint subtitle1 = new Paint();
        subtitle1.setColor(Color.BLACK);
        subtitle1.setTextSize(40);
        subtitle1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        Paint subtitle2 = new Paint();
        subtitle2.setColor(Color.BLACK);
        subtitle2.setTextSize(40);
        subtitle2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        Toast.makeText(this, getResources().getDimensionPixelSize(R.dimen.big_size) + "", Toast.LENGTH_SHORT).show();
        c.drawColor(Color.WHITE);

//        if(qr==null) {
        c.drawBitmap(qr, 0, 0, null);
        c.drawText("Laundry super ngok", 300, 50 + 40, title);
        c.drawText("Jalan niaga no 24", 300, 50 + 40 + 50 + 20, subtitle1);
        c.drawText("Malang - 192.168.100.1", 300, 50 + 40 + 50 + 20 + 50 + 20, subtitle2);
//        } else {
//            c.drawText("Laundry super ngok", 40, 50 + 40, title);
//            c.drawText("Jalan niaga no 24", 40, 50 + 40 + 50 + 20, subtitle1);
//            c.drawText("Malang - 192.168.100.1", 40, 50 + 40 + 50 + 20 + 50 + 20, subtitle2);
//        }
        iv.setImageBitmap(layer);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.header, null, false);
                view.setDrawingCacheEnabled(true);

                view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                view.layout(0, 0, view.getMeasuredWidth(),view.getMeasuredHeight());
                view.buildDrawingCache();
                Bitmap bm = view.getDrawingCache();
                BluetoothPrint.Builder builder = new BluetoothPrint.Builder(BluetoothPrint.Size.WIDTH80);
                builder.addTextln("1111");
                builder.addTextln("2222");
                builder.addBitmap(bm);
                builder.addTextln("3333");
                builder.addTextln("4444");
                iv.setImageBitmap(bm);
                BluetoothPrint.with(MainActivity.this)
                        .setData(builder)
                        .print();

            }
        });
    }

    public static Bitmap QRCode(String str, int w, int h) {
        Bitmap bitmap = null;
        final int WHITE = 0xFFFFFFFF;
        final int BLACK = 0xFF000000;
        final int WIDTH = w;
        final int HEIGHT = h;
        int newHeight = 0, newWidth = 0;
        int[] pixels = null;
        try {
            BitMatrix result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
            newHeight = result.getHeight();
            newWidth = result.getWidth();
            pixels = new int[newHeight * newWidth];
            for (int y = 0; y < newHeight; y++) {
                int offset = y * newWidth;
                for (int x = 0; x < newWidth; x++) {
                    pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
                }
            }

        } catch (WriterException e) {
            e.printStackTrace();
        }

        bitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, newWidth, 0, 0, newWidth, newHeight);
        return bitmap;
    }
}