package org.alkaaf.btprint;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.alkaaf.btprint.MainSdk.BluetoothService;

import static org.alkaaf.btprint.MainSdk.BluetoothService.DEVICE_NAME;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_CONNECTION_LOST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_DEVICE_NAME;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_READ;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_STATE_CHANGE;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_TOAST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_UNABLE_CONNECT;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_WRITE_END;
import static org.alkaaf.btprint.MainSdk.BluetoothService.STATE_CONNECTED;
import static org.alkaaf.btprint.MainSdk.BluetoothService.TOAST;

/**
 * Created by dalbo on 3/14/2018.
 */

public class LiveService extends Service {
    class LiveBinder extends Binder {
        public LiveService getInstance() {
            return LiveService.this;
        }
    }

    interface PrinterAction {
        void onStateChange(int state);

        void onMessage(int messageType, String misc);

        void onMiscMessage(boolean result, String msg);
    }


    class BTHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String misc = "";
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (printerAction != null) printerAction.onStateChange(msg.arg1);
                    if (msg.arg1 == STATE_CONNECTED && printAfterConnect && printData != null) {
                        bluetoothService.write(printData);
                        printAfterConnect = false;
                    }
                    break;
                case MESSAGE_WRITE_END:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    misc = msg.getData().getString(DEVICE_NAME);
                    break;
                case MESSAGE_TOAST:
                    misc = msg.getData().getString(TOAST);
                    break;
                case MESSAGE_CONNECTION_LOST:
                    misc = msg.getData().getString(TOAST);
                    break;
                case MESSAGE_UNABLE_CONNECT:
                    misc = msg.getData().getString(TOAST) + ": " + msg.getData().getString(DEVICE_NAME);
                    break;
            }
            if (printerAction != null) printerAction.onMessage(msg.what, misc);
        }
    }

    public BluetoothService bluetoothService;
    public BluetoothPreferences pref;
    LiveBinder binder = new LiveBinder();
    BTHandler handler = new BTHandler();
    PrinterAction printerAction = null;
    BluetoothAdapter btAdapter;
    byte[] printData = null;
    boolean printAfterConnect = false;
    CountDownTimer autoDcTimer;
    long autoDcTick = -1;

    public int getState() {
        return bluetoothService.getState();
    }

    public void setPrinterAction(PrinterAction printerAction) {
        this.printerAction = printerAction;
    }

    public void removePrinterAction() {
        this.printerAction = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bluetoothService = new BluetoothService(this, handler);
        pref = new BluetoothPreferences(this);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (autoDcTimer != null) {
                autoDcTimer.cancel();
            }
            autoDcTick = intent.getLongExtra(PrintController.FLAG_AUTO_DC, -1);
            if (autoDcTick > 0L) {
                autoDcTimer = new CountDownTimer(intent.getLongExtra(PrintController.FLAG_AUTO_DC, -1), 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.i("AUTO_DC", millisUntilFinished + "");
                    }

                    @Override
                    public void onFinish() {
                        disconnect();
                    }
                };
            }
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bluetoothService != null) {
            bluetoothService.stop();
        }
    }

    public void disconnect() {
        if (bluetoothService != null) {
            bluetoothService.stop();
        }
    }

    public void startAutoDc() {
        if (autoDcTick > 0 && autoDcTimer != null) {
            autoDcTimer.start();
        }

    }

    // starting here are control methods
    public void connect() {
        String btAddr = pref.getString(BluetoothPreferences.Key.BT_ADDR);
        if (btAddr == null) {
            if (printerAction != null)
                printerAction.onMiscMessage(false, "Belum ada printer disimpan. Harap melakukan pemindaian perangkat");
        } else {
            if (BluetoothAdapter.checkBluetoothAddress(btAddr)) {
                BluetoothDevice dev = btAdapter.getRemoteDevice(btAddr);
                bluetoothService.connect(dev);
            } else {
                Toast.makeText(this, "Alamat bluetooth tidak valid", Toast.LENGTH_SHORT).show();
                if (printerAction != null)
                    printerAction.onMiscMessage(false, "Alamat bluetooth tidak valid");
            }
        }
    }

    public LiveService setPrintData(byte[] b) {
        printData = b;
        return this;
    }

    public void print() {
        if (getState() == BluetoothService.STATE_CONNECTED && printData != null) {
            bluetoothService.write(printData);
        } else {
            connect();
            printAfterConnect = true;
        }
    }
}
