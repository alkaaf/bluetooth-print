package org.alkaaf.btprint;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import java.util.Map;
import java.util.Set;

/**
 * Created by dalbo on 1/10/2018.
 */

public class BluetoothPreferences {
    public static enum Key {
        BT_ADDR("bluetooth.addr"),
        BT_NAME("bluetooth.name"),
        BT_LEN("bluetooth.len"),
        BT_WIDTH("bluetooth.size"),
        BT_PRINT_NAME("bluetooth.print.name");

        public String value;

        Key(String value) {
            this.value = value;
        }
    }

    SharedPreferences sp;
    SharedPreferences.Editor edit;
    public static final String PREF_NAME = "menej.in_bluetooth_pref";

    public BluetoothPreferences(Context context) {
        sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        edit = sp.edit();
    }

    public String getString(Key key) {
        return sp.getString(key.value, null);
    }

    public String getString(Key key, String defVal) {
        return sp.getString(key.value, defVal);
    }

    public BluetoothPreferences putString(Key key, String value) {
        edit.putString(key.value, value);
        return this;
    }

    public int getInt(Key key) {
        return sp.getInt(key.value, 0);
    }

    public int getInt(Key key, int defVal) {
        return sp.getInt(key.value, defVal);
    }

    public BluetoothPreferences putInt(Key key, int value) {
        edit.putInt(key.value, value);
        return this;
    }

    public BluetoothPreferences putSize(BluetoothPrint.Size size) {
        return putInt(Key.BT_LEN, size.len)
                .putInt(Key.BT_WIDTH, size.width)
                .putString(Key.BT_PRINT_NAME, size.printName);
    }

    public void commit() {
        edit.commit();
    }

    public static BluetoothPreferences instance(Context context) {
        return new BluetoothPreferences(context);
    }
}
