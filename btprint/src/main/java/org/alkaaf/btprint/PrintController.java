package org.alkaaf.btprint;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.alkaaf.btprint.MainSdk.BluetoothService;
import org.alkaaf.btprint.Model.BluetoothDev;

import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_CONNECTION_LOST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_DEVICE_NAME;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_READ;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_STATE_CHANGE;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_TOAST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_UNABLE_CONNECT;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_WRITE_END;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_WRITE_START;

/**
 * Created by dalbo on 3/14/2018.
 */

public class PrintController extends ActivityBase {
    public static final String FLAG_CLOSE_DELAY_AFTER_FINISH = "flag.close.after.finish";
    public static final String FLAG_AUTO_DC = "flag.auto.dc";
    BluetoothAdapter btAdapter;
    BluetoothService btService;
    BluetoothDev selectedDevice = null;
    BluetoothPreferences sp;
    TextView tvPesan, tvBtDev, tvAutoClose;
    Button bScanMore, bPrintNow;
    ImageView icon;
    AlertDialog dialogScanLagi;
    final String TAG = "BT_PREPARE";
    public static final int REQ_ENABLE_BT = 0;
    public static final int REQ_SCAN_DEV = 1;
    public static String INTENT_DATA_PRINT = "dataprint";
    public static String INTENT_BLUETOOTHPRINT_DATA = "bluetoothprint.data";
    public static final String BLUETOOTH_ADDRESS = "bluetooth_address";
    public static final int REQ_ACCESS_FINE_LOCATION = 24;
    byte[] dataPrint = null;
    Intent intentService;
    LiveService liveService;
    LiveService.PrinterAction action = new LiveService.PrinterAction() {
        @Override
        public void onStateChange(final int state) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (state) {
                        case BluetoothService.STATE_NO_BLUETOOTH:
                            icon.setColorFilter(Color.RED);
                            tvPesan.setText("Bluetooth mati");
                            bPrintNow.setEnabled(true);
                            break;

                        case BluetoothService.STATE_CONNECTED:
                            icon.setColorFilter(Color.GREEN);
                            tvPesan.setText("Terhubung");
                            bPrintNow.setEnabled(true);
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            icon.setColorFilter(Color.YELLOW);
                            tvPesan.setText("Menghubungkan");
                            break;
                        case BluetoothService.STATE_LISTEN:
                            bPrintNow.setEnabled(false);
                            icon.setColorFilter(Color.RED);
                            tvPesan.setText("Terputus");
                            cd = new CountDownTimer(5000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    bPrintNow.setText("CETAK (" + millisUntilFinished / 1000 + ")");
                                }

                                @Override
                                public void onFinish() {
                                    bPrintNow.setText("CETAK");
                                    bPrintNow.setEnabled(true);
                                }
                            }.start();
                            break;
                        case BluetoothService.STATE_NONE:
                            icon.setColorFilter(Color.GRAY);
                            tvPesan.setText("Tidak terhubung");
                            bPrintNow.setEnabled(true);
                            break;
                    }
                }
            });

        }

        @Override
        public void onMessage(final int messageType, final String misc) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (messageType) {
                        case MESSAGE_STATE_CHANGE:
                            break;
                        case MESSAGE_WRITE_START:
                            bPrintNow.setEnabled(false);
                            break;
                        case MESSAGE_WRITE_END:
                            if (liveService.getState() == BluetoothService.STATE_CONNECTED && getIntent().getLongExtra(FLAG_CLOSE_DELAY_AFTER_FINISH, -1) > -1) {
                                long ms = getIntent().getLongExtra(FLAG_CLOSE_DELAY_AFTER_FINISH, 0);
                                new CountDownTimer(ms, 1000) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        tvAutoClose.setText("Ditutup setelah " + millisUntilFinished / 1000 + " detik");
                                    }

                                    @Override
                                    public void onFinish() {
                                        finish();
                                    }
                                }.start();
                            }
                            bPrintNow.setEnabled(true);
                            break;
                        case MESSAGE_READ:
                            break;
                        case MESSAGE_DEVICE_NAME:
                            break;
                        case MESSAGE_TOAST:
                            tvPesan.setText(misc);
                            break;
                        case MESSAGE_CONNECTION_LOST:
                            tvPesan.setText(misc);
                            bPrintNow.setEnabled(true);
                            break;
                        case MESSAGE_UNABLE_CONNECT:
                            tvPesan.setText(misc);
                            bPrintNow.setEnabled(true);
                            break;
                    }
                }
            });

        }

        @Override
        public void onMiscMessage(boolean result, final String msg) {
         runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 Toast.makeText(PrintController.this, msg, Toast.LENGTH_SHORT).show();
             }
         });

        }
    };
    ServiceConnection svcConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            liveService = ((LiveService.LiveBinder) service).getInstance();
            liveService.setPrinterAction(action);
            bPrintNow.setEnabled(true);
            stateChange(liveService.getState());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            liveService = null;
            liveService.removePrinterAction();
            bPrintNow.setEnabled(false);
        }
    };

    CountDownTimer cd;

    public void stateChange(int state) {
        switch (state) {
            case BluetoothService.STATE_NO_BLUETOOTH:
                icon.setColorFilter(Color.RED);
                tvPesan.setText("Bluetooth mati");
                bPrintNow.setEnabled(true);
                break;
            case BluetoothService.STATE_CONNECTED:
                icon.setColorFilter(Color.GREEN);
                tvPesan.setText("Terhubung");
                bPrintNow.setEnabled(true);
                break;
            case BluetoothService.STATE_CONNECTING:
                icon.setColorFilter(Color.YELLOW);
                tvPesan.setText("Menghubungkan");
                break;
            case BluetoothService.STATE_LISTEN:
                icon.setColorFilter(Color.RED);
                tvPesan.setText("Terputus");
                bPrintNow.setEnabled(true);
                break;
            case BluetoothService.STATE_NONE:
                icon.setColorFilter(Color.GRAY);
                tvPesan.setText("Tidak terhubung");
                bPrintNow.setEnabled(true);
                break;
        }
    }

    private void reloadText() {
        if (sp.getString(BluetoothPreferences.Key.BT_ADDR) == null) {
            tvBtDev.setText("(Belum ada perangkat tersimpan)");
            bPrintNow.setEnabled(false);
        } else {
            tvBtDev.setText(sp.getString(BluetoothPreferences.Key.BT_NAME) + " (" + sp.getString(BluetoothPreferences.Key.BT_ADDR) + ")");
            bPrintNow.setEnabled(true);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prepare);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Print via Bluetooth");
        }
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        dataPrint = getIntent().getByteArrayExtra(INTENT_DATA_PRINT);
        tvPesan = findViewById(R.id.pesan);
        tvBtDev = findViewById(R.id.btDev);
        tvAutoClose = findViewById(R.id.tvAutoClose);
        bScanMore = findViewById(R.id.bScanMore);
        bPrintNow = findViewById(R.id.bPrintNow);
        icon = findViewById(R.id.icon);
        intentService = new Intent(this, LiveService.class);
        intentService.putExtra(FLAG_AUTO_DC, getIntent().getLongExtra(FLAG_AUTO_DC, -1));
        sp = BluetoothPreferences.instance(this);

        bScanMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showScanActivity();
            }
        });
        dialogScanLagi = new AlertDialog.Builder(this)
                .setTitle("Perhatian")
                .setMessage("Gagal dalam menghubung ke perangkat, apakah anda ingin memindai perangkat baru?")
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showScanActivity();
                    }
                })
                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogScanLagi.dismiss();
                    }
                })
                .create();
        bPrintNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btAdapter.isEnabled()) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            liveService.setPrintData(dataPrint).print();
                        }
                    }).start();

                } else {
                    askBluetooth();
                }
                bPrintNow.setEnabled(false);
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this, "Aplikasi tidak dapat melanjutkan karena tidak diijinkan untuk melakukan pemindaian perangkat bluetooth", Toast.LENGTH_LONG).show();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }
        }
    }

    public void showScanActivity() {
        Intent intentScanDev = new Intent(this, ActivityDeviceList.class);
        startActivityForResult(intentScanDev, REQ_SCAN_DEV);
    }

    public void askBluetooth() {
        Intent intentEnableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intentEnableBT, REQ_ENABLE_BT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        reqperm();
        askBluetooth();
        startService(intentService);
        bindService(intentService, svcConn, Context.BIND_AUTO_CREATE);
        reloadText();
    }

    private void reqperm(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQ_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(liveService!=null) {
            liveService.startAutoDc();
            unbindService(svcConn);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_ENABLE_BT: {
                if (resultCode == Activity.RESULT_OK) {
                } else {
                    Toast.makeText(this, "Mohon aktifkan bluetooth untuk melakukan pencetakan", Toast.LENGTH_SHORT).show();
                }
                bPrintNow.setEnabled(true);
                break;
            }
            case REQ_SCAN_DEV: {
                if (resultCode == Activity.RESULT_OK) {
                    // getselected device
                    selectedDevice = data.getParcelableExtra(ActivityDeviceList.INTENT_BT_DATA);
                    sp.putString(BluetoothPreferences.Key.BT_ADDR, selectedDevice.getAddr())
                            .putString(BluetoothPreferences.Key.BT_NAME, selectedDevice.getName())
                            .commit();
                    reloadText();
                    liveService.bluetoothService.stop();
                }
                break;
            }
        }
    }
}
