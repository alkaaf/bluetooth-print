package org.alkaaf.btprint;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;

import org.alkaaf.btprint.Command.Command;
import org.alkaaf.btprint.Command.PrintPicture;
import org.alkaaf.btprint.Command.PrinterCommand;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by dalbo on 4/6/2017.
 */

public class BluetoothPrint {
    public static Printable staticPrintable;

    public enum Size {
        WIDTH58(32, 384, "58 mm"),
        WIDTH80(48, 576, "80 mm"),
        WIDTH100(64, 768, "100 mm");
        public int width;
        public int len;
        public String printName;

        Size(int len, int width, String printName) {
            this.len = len;
            this.width = width;
            this.printName = printName;
        }
    }

    public enum Cmd {
        RESET(new byte[]{0x1b, 0x40, 0x0a}),
        BOLD_ENABLE(new byte[]{0x1b, 0x45, 0x01}),
        BOLD_DISABLE(new byte[]{0x1b, 0x45, 0x00}),
        FEED(new byte[]{0x0a}), // print and feed the paper
        FONT_STANDART(new byte[]{0x1b, 0x4d, 0x00}), // standard ASCII font
        FONT_COMPRESS(new byte[]{0x1b, 0x4d, 0x01}), // Compress ASCII font
        FONT_ENLARGE(new byte[]{0x1d, 0x21, 0x00}), // font is not enlarged
        FONT_LARGE1(new byte[]{0x1d, 0x21, 0x11}), // double width / height
        FONT_LARGE2(new byte[]{0x1d, 0x21, 0x22}), //double width / height
        FONT_LARGE3(new byte[]{0x1d, 0x21, 0x33}), //double width / height
        REVERSE_PRINT_DISABLE(new byte[]{0x1b, 0x7b, 0x00}), // Cancel the reverse printing
        REVERSE_PRINT_ENABLE(new byte[]{0x1b, 0x7b, 0x01}), // Select Inverted Print
        INVERSE_COLOR_DISABLE(new byte[]{0x1d, 0x42, 0x00}), // cancel the black and white anti-display
        INVERSE_COLOR_ENABLE(new byte[]{0x1d, 0x42, 0x01}), // select black and white anti-display
        ROTATE_90_DISABLE(new byte[]{0x1b, 0x56, 0x00}), // cancel rotation 90 ° clockwise
        ROTATE_90_ENABLE(new byte[]{0x1b, 0x56, 0x01}), // Select to rotate 90 ° clockwise
        INT_CUTTER(new byte[]{0x0a, 0x1d, 0x56, 0x42, 0x01, 0x0a}), // cutter instructions
        INT_BEEP(new byte[]{0x1b, 0x42, 0x03, 0x03}), // Beep command
        INT_CASE(new byte[]{0x1b, 0x70, 0x00, 0x50, 0x50}), // CASE instructions
        DRAWER(new byte[]{0x10, 0x14, 0x00, 0x05, 0x05}), // real-time cash drawer instruction
        MODE_CHAR(new byte[]{0x1c, 0x2e}), // enter character mode
        MODE_CHINESE(new byte[]{0x1c, 0x26}), // ​​enter the Chinese mode
        PRINT_TEST(new byte[]{0x1f, 0x11, 0x04}), // print self-test page
        DISABLE_BUTTON_ENABLE(new byte[]{0x1b, 0x63, 0x35, 0x01}), // disable the button
        DISABLE_BUTTON_DISABLE(new byte[]{0x1b, 0x63, 0x35, 0x00}), // cancel prohibit button
        UNDERLINE_ENABLE(new byte[]{0x1b, 0x2d, 0x02, 0x1c, 0x2d, 0x02}), // set underline
        UNDERLINE_DISABLE(new byte[]{0x1b, 0x2d, 0x00, 0x1c, 0x2d, 0x00}), // cancel the underline
        MODE_HEX(new byte[]{0x1f, 0x11, 0x03}); // The printer enters hexadecimal mode

        public byte[] val;

        Cmd(byte[] val) {
            this.val = val;
        }
    }

    Activity activity;
    byte[] dataPrint;
    Intent intentPrintBt;
    Intent intentPrintUsb;

    public BluetoothPrint(Activity activity) {
        this.activity = activity;
        intentPrintBt = new Intent(activity, PrintController.class);
        intentPrintUsb = new Intent(activity, ActivityUSB.class);
    }

    public BluetoothPrint setData(byte[] b) {
        intentPrintBt.putExtra(PrintController.INTENT_DATA_PRINT, b);
        intentPrintUsb.putExtra(ActivityUSB.INTENT_DATA_PRINT, b);
        return this;
    }

    public BluetoothPrint setData(Builder builder) {
        intentPrintBt.putExtra(PrintController.INTENT_DATA_PRINT, builder.getByte());
        intentPrintUsb.putExtra(ActivityUSB.INTENT_DATA_PRINT, builder.getByte());
        return this;
    }

    public BluetoothPrint autoCloseAfter(long delayMs) {
        intentPrintBt.putExtra(PrintController.FLAG_CLOSE_DELAY_AFTER_FINISH, delayMs);
        return this;
    }

    public BluetoothPrint autoDcAfter(long delayMs) {
        intentPrintBt.putExtra(PrintController.FLAG_AUTO_DC, delayMs);
        return this;
    }

    public void print() {
        new AlertDialog.Builder(activity).setTitle("Pilih jenis printer")
                .setItems(new String[]{"Bluetooth", "USB"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: {
                                activity.startActivity(intentPrintBt);
                                break;
                            }
                            case 1: {
                                activity.startActivity(intentPrintUsb);
                                break;
                            }
                            default: {

                            }
                        }
                    }
                })
                .show();

    }

    public void print(int requestCode) {
        activity.startActivityForResult(intentPrintBt, requestCode);
    }

    @Deprecated
    public void print2(byte[] data) {
        Intent intent = new Intent(activity, PrintController.class);
        intent.putExtra(PrintController.INTENT_DATA_PRINT, data);
        activity.startActivity(intent);
    }

    @Deprecated
    public void print(byte[] data) {
        Intent intent = new Intent(activity, ActivityPrepareBluetooth.class);
        intent.putExtra(ActivityPrepareBluetooth.INTENT_DATA_PRINT, data);
        intent.putExtra(ActivityPrepareBluetooth.PRINT_TYPE, ActivityPrepareBluetooth.PRINT_DIRECT);
        activity.startActivity(intent);
    }

    @Deprecated
    public void print(byte[] data, int requestCode) {
        Intent intent = new Intent(activity, ActivityPrepareBluetooth.class);
        intent.putExtra(ActivityPrepareBluetooth.INTENT_DATA_PRINT, data);
        intent.putExtra(ActivityPrepareBluetooth.PRINT_TYPE, ActivityPrepareBluetooth.PRINT_DIRECT);
        activity.startActivityForResult(intent, requestCode);
    }

    @Deprecated
    public void print(Printable printable) {
        staticPrintable = printable;
        Intent intent = new Intent(activity, ActivityPrepareBluetooth.class);
        intent.putExtra(ActivityPrepareBluetooth.PRINT_TYPE, ActivityPrepareBluetooth.PRINT_STATIC);
        activity.startActivity(intent);
    }

    /**
     * Created by dalbo on 9/5/2017.
     */

    public static class Builder {
        String space = " ";
        ByteArrayOutputStream baos;

        public static int WIDTH58 = 384;
        public static int WIDTH80 = 576;
        public static int WIDTH100 = 768;

        public static int LEN58 = 32;
        public static int LEN80 = 48;
        public static int LEN100 = 64;
        public static Size DEFAULT = Size.WIDTH80;
        int len, width;

        public Builder(Size size) {
            baos = new ByteArrayOutputStream();
            this.len = size.len;
            this.width = size.width;
        }

        public Builder(int len, int width) {
            this.baos = new ByteArrayOutputStream();
            this.len = len;
            this.width = width;
        }

        public Builder(Context context) {
            baos = new ByteArrayOutputStream();
            BluetoothPreferences bp = new BluetoothPreferences(context);
            this.len = bp.getInt(BluetoothPreferences.Key.BT_LEN, DEFAULT.len);
            this.width = bp.getInt(BluetoothPreferences.Key.BT_WIDTH, DEFAULT.width);
        }

        public void setCommand(Cmd cmd) {
            try {
                baos.write(cmd.val);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void setCommand(byte[] cmd){
            try {
                baos.write(cmd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addLine() {
            for (int i = 0; i < len; i++) {
                addText("-");
            }
            addLF();
        }

        public void addFrontEnd(String front, String end) {
            addText(createPad(front, len - end.length(), false));
            addText(end);
            addLF();
        }

        public void setAlignLeft() {
            try {
                baos.write(Command.ESC_Align);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void setAlignMid() {
            try {
                baos.write(Command.ESC_Align_Mid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void setAlignRight() {
            try {
                baos.write(Command.ESC_Align_Right);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addText(String text) {
            try {
                baos.write(text.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addItem(String item, double qty) {
            addTextln(item + "(" + Math.round(qty) + ")");
        }

        public void addBitmap(Bitmap bm) {
            try {
                int paperwidth = width;
//
//                byte[] feedPaper = PrinterCommand.POS_Set_PrtAndFeedPaper(30);
//                byte[] cut = PrinterCommand.POS_Set_Cut(1);
                baos.write(Command.ESC_Init);
                baos.write(PrintPicture.POS_PrintBMP(bm, paperwidth, 0));
                baos.write(Command.ESC_Align);
//                if (feedPaper != null && cut != null) {
//                    baos.write(feedPaper);
//                    baos.write(cut);
//                }
                baos.write(PrinterCommand.POS_Set_PrtInit());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Deprecated
        public void addBitmap(Bitmap qr, int paperwidth) {
            try {
//                byte[] feedPaper = PrinterCommand.POS_Set_PrtAndFeedPaper(30);
//                byte[] cut = PrinterCommand.POS_Set_Cut(1);
                baos.write(Command.ESC_Init);
                baos.write(PrintPicture.POS_PrintBMP(qr, paperwidth, 0));
                baos.write(Command.ESC_Align);
//                if (feedPaper != null && cut != null) {
//                    baos.write(feedPaper);
//                    baos.write(cut);
//                }
                baos.write(PrinterCommand.POS_Set_PrtInit());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addQr(String qrString, int zoom) {
            try {
//                byte[] feedPaper = PrinterCommand.POS_Set_PrtAndFeedPaper(30);
//                byte[] cut = PrinterCommand.POS_Set_Cut(1);
                byte[] qrcode = PrinterCommand.getBarCommand(qrString, 0, 3, 4);
                baos.write(Command.ESC_Align_Mid);
                if (qrcode != null) {
                    baos.write(qrcode);
                }
                baos.write(Command.ESC_Align);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public String createPad(String in, int fullLen, boolean inFront) {
            if (in.length() > fullLen) {
                return in;
            } else {
                String sp = "";
                for (int i = 0; i < fullLen - in.length(); i++) {
                    sp += space;
                }

                return inFront ? sp + in : in + sp;
            }
        }

        public String createPad(char c, String in, int fullLen, boolean inFront) {
            if (in.length() > fullLen) {
                return in;
            } else {
                String sp = " ";
                for (int i = 0; i < fullLen - in.length(); i++) {
                    sp += c;
                }

                return inFront ? sp + in : in + sp;
            }
        }

        public void addTextln(String text) {
            try {
                baos.write(text.getBytes());
                addLF();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public void addLF() {
            try {
                baos.write(Command.LF);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addLF(int n) {
            try {
                for (int i = 0; i < n; i++) {
                    baos.write(Command.LF);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public byte[] getByte() {
            return baos.toByteArray();
        }
    }


    public static BluetoothPrint with(Activity activity) {
        return new BluetoothPrint(activity);
    }

    public static Class<LiveService> getServiceClass() {
        return LiveService.class;
    }
}