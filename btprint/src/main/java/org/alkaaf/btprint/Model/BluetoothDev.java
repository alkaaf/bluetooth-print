package org.alkaaf.btprint.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dalbo on 4/5/2017.
 */

public class BluetoothDev implements Parcelable {
    String name;
    String addr;

    public BluetoothDev(String name, String addr) {
        this.name = name;
        this.addr = addr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.addr);
    }

    protected BluetoothDev(Parcel in) {
        this.name = in.readString();
        this.addr = in.readString();
    }

    public static final Creator<BluetoothDev> CREATOR = new Creator<BluetoothDev>() {
        @Override
        public BluetoothDev createFromParcel(Parcel source) {
            return new BluetoothDev(source);
        }

        @Override
        public BluetoothDev[] newArray(int size) {
            return new BluetoothDev[size];
        }
    };
}
