package org.alkaaf.btprint;

/**
 * Created by dalbo on 1/11/2018.
 */

public interface Printable {
    byte[] getPrint(int len, int width);
}
