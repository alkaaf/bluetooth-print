package org.alkaaf.btprint;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.alkaaf.btprint.Model.BluetoothDev;

import java.util.Set;

/**
 * Created by dalbo on 4/5/2017.
 */

public class ActivityDeviceList extends ActivityBase {
    ListView lvPaired;
    ListView lvNew;
    Button bScan;

    IntentFilter filter1;
    IntentFilter filter2;
    IntentFilter filter;
    public static final String INTENT_BT_DATA = "selectedBT";

    ArrayAdapter<BluetoothDev> adapterPaired;
    ArrayAdapter<BluetoothDev> adapterNew;

    BluetoothAdapter bluetoothAdapter;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice nd = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (newDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
                    if (!checkIfExist(newDevice)) {
                        adapterNew.add(new BluetoothDev(newDevice.getName(), newDevice.getAddress()));
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                bScan.setEnabled(true);
                bScan.setText("Scan");
            }
        }
    };

    private boolean checkIfExist(BluetoothDevice dev) {
        for (int i = 0; i < adapterNew.getCount(); i++) {
            if (adapterNew.getItem(i).getAddr().equals(dev.getAddress())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param savedInstanceState
     * @BindView(R.id.lvPaired) ListView lvPaired;
     * @BindView(R.id.lvNew) ListView lvNew;
     * @BindView(R.id.bScan) Button bScan;
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        lvPaired = findViewById(R.id.lvPaired);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Pilih printer Bluetooth");
        }
        lvNew = findViewById(R.id.lvNew);
        bScan = findViewById(R.id.bScan);
        setResult(Activity.RESULT_CANCELED);
        filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);

//        registerReceiver(receiver, filter2);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        adapterPaired = new ArrayAdapter<BluetoothDev>(this, R.layout.device) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device, parent, false);
                TextView tvName = v.findViewById(R.id.tvDevName);
                TextView tvAddr = v.findViewById(R.id.tvDevAddr);
                tvName.setText(getItem(position).getName());
                tvAddr.setText(getItem(position).getAddr());
                return v;
            }
        };
        adapterNew = new ArrayAdapter<BluetoothDev>(this, R.layout.device) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device, parent, false);
                TextView tvName = v.findViewById(R.id.tvDevName);
                TextView tvAddr = v.findViewById(R.id.tvDevAddr);
                tvName.setText(getItem(position).getName());
                tvAddr.setText(getItem(position).getAddr());
                return v;
            }
        };
        lvPaired.setAdapter(adapterPaired);
        lvPaired.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.putExtra(INTENT_BT_DATA, adapterPaired.getItem(i));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        lvNew.setAdapter(adapterNew);
        lvNew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.putExtra(INTENT_BT_DATA, adapterNew.getItem(i));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        Set<BluetoothDevice> pairedDevs = bluetoothAdapter.getBondedDevices();
        if (pairedDevs.size() > 0) {
            for (BluetoothDevice pairedDev : pairedDevs) {
                adapterPaired.add(new BluetoothDev(pairedDev.getName(), pairedDev.getAddress()));
                if (isActive) {
                    adapterPaired.notifyDataSetChanged();
                }
            }
        }
        bScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doDiscovery();
            }
        });
    }

    public void doDiscovery() {
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        bScan.setText("Scanning");
        bScan.setEnabled(false);
        adapterNew.clear();
        bluetoothAdapter.startDiscovery();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
