package org.alkaaf.btprint;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.alkaaf.btprint.Command.Command;
import org.alkaaf.btprint.MainSdk.BluetoothService;
import org.alkaaf.btprint.Model.BluetoothDev;

import static org.alkaaf.btprint.MainSdk.BluetoothService.DEVICE_NAME;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_CONNECTION_LOST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_DEVICE_NAME;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_READ;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_STATE_CHANGE;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_TOAST;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_UNABLE_CONNECT;
import static org.alkaaf.btprint.MainSdk.BluetoothService.MESSAGE_WRITE_END;
import static org.alkaaf.btprint.MainSdk.BluetoothService.STATE_NONE;
import static org.alkaaf.btprint.MainSdk.BluetoothService.TOAST;

/**
 * Created by dalbo on 4/5/2017.
 */

public class ActivityPrepareBluetooth extends ActivityBase {
    final boolean DEBUG = true;
    final String TAG = "BT_PREPARE";
    public static final int REQ_ENABLE_BT = 0;
    public static final int REQ_SCAN_DEV = 1;
    public static String INTENT_DATA_PRINT = "dataprint";
    public static String INTENT_BLUETOOTHPRINT_DATA = "bluetoothprint.data";
    public static final String BLUETOOTH_ADDRESS = "bluetooth_address";

    public static final String PRINT_TYPE = "print.type";
    public static final int PRINT_DIRECT = 1;
    public static final int PRINT_STATIC = 2;

    public static final int REQ_ACCESS_FINE_LOCATION = 24;
    BluetoothAdapter btAdapter;
    BluetoothService btService;
    BluetoothDev selectedDevice = null;
    BluetoothPreferences sp;
    TextView tvPesan, tvBtDev;
    Button bScanMore, bPrintNow;
    AlertDialog dialogScanLagi;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!isFinishing()) {
                switch (msg.what) {
                    case MESSAGE_STATE_CHANGE:
                        if (DEBUG)
                            Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                        switch (msg.arg1) {
                            case BluetoothService.STATE_CONNECTED:
                                tvPesan.setText("Mencetak");
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (btService != null) {
                                            btService.stop();
                                        }
                                    }
                                }).start();
                                print();

                                break;
                            case BluetoothService.STATE_CONNECTING:
                                tvPesan.setText("Menghubungkan");
                                break;
                            case BluetoothService.STATE_LISTEN:
                                break;
                            case BluetoothService.STATE_NONE:
                                break;
                            case BluetoothService.STATE_CONNECT_FAIL:
                                tvPesan.setText("Gagal menghubungi perangkat");
                                bPrintNow.setEnabled(true);
                                dialogScanLagi.show();
                                break;
                        }
                        break;
                    case MESSAGE_WRITE_END:

                        break;
                    case MESSAGE_READ:

                        break;
                    case MESSAGE_DEVICE_NAME:
                        String mConnectedDeviceName;
                        mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
//                        Toast.makeText(getApplicationContext(), "Terkoneksi ke " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                        tvPesan.setText("Terkoneksi ke " + mConnectedDeviceName);
                        break;
                    case MESSAGE_TOAST:
                        Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                        tvPesan.setText("Siap!");
                        bPrintNow.setEnabled(true);
                        break;
                    case MESSAGE_CONNECTION_LOST:
                        Toast.makeText(getApplicationContext(), "Pencetakan selesai", Toast.LENGTH_SHORT).show();
                        bPrintNow.setEnabled(true);
                        break;
                    case MESSAGE_UNABLE_CONNECT:
//                        Toast.makeText(getApplicationContext(), "Tidak dapat menghubungi perangkat", Toast.LENGTH_SHORT).show();
                        String dev;
                        dev = msg.getData().getString(DEVICE_NAME);
                        tvPesan.setText("Tidak dapat menyambung ke " + dev);
                        bPrintNow.setEnabled(true);
                        dialogScanLagi.show();
                        break;
                }
            }
        }
    };
    private byte[] dataPrint;

    private void reloadText() {
        if (sp.getString(BluetoothPreferences.Key.BT_ADDR) == null) {
            tvBtDev.setText("(Belum ada perangkat tersimpan)");
            bPrintNow.setEnabled(false);
        } else {
            tvBtDev.setText(sp.getString(BluetoothPreferences.Key.BT_NAME) + " (" + sp.getString(BluetoothPreferences.Key.BT_ADDR) + ")");
            bPrintNow.setEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        askBluetooth();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prepare);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Print");
        }

        tvPesan = findViewById(R.id.pesan);
        tvBtDev = findViewById(R.id.btDev);
        bScanMore = findViewById(R.id.bScanMore);
        bPrintNow = findViewById(R.id.bPrintNow);

        sp = BluetoothPreferences.instance(this);
        reloadText();
        dialogScanLagi = new AlertDialog.Builder(this)
                .setTitle("Perhatian")
                .setMessage("Gagal dalam menghubung ke perangkat, apakah anda ingin memindai perangkat baru?")
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showScanActivity();
                    }
                })
                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogScanLagi.dismiss();
                    }
                })
                .create();

        dataPrint = getIntent().getByteArrayExtra(INTENT_DATA_PRINT);
//        if (dataPrint == null) {
//            finish();
//        }

        btService = new BluetoothService(this, mHandler);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQ_ACCESS_FINE_LOCATION);
        }
        bScanMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showScanActivity();
            }
        });
        bPrintNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!btAdapter.isEnabled()) {
                    askBluetooth();
                } else {
                    connect();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this, "Aplikasi tidak dapat melanjutkan karena tidak diijinkan untuk melakukan pemindaian perangkat bluetooth", Toast.LENGTH_LONG).show();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }
        }
    }

    public void showScanActivity() {
        Intent intentScanDev = new Intent(this, ActivityDeviceList.class);
        startActivityForResult(intentScanDev, REQ_SCAN_DEV);
    }


    public void askBluetooth() {
        Intent intentEnableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intentEnableBT, REQ_ENABLE_BT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_ENABLE_BT: {
                if (resultCode == Activity.RESULT_OK) {
//                    connect();
                } else {
                    Toast.makeText(this, "Mohon aktifkan bluetooth untuk melakukan pencetakan", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            }
            case REQ_SCAN_DEV: {
                if (resultCode == Activity.RESULT_OK) {
                    // getselected device
                    selectedDevice = data.getParcelableExtra(ActivityDeviceList.INTENT_BT_DATA);
                    // simpen to iku
//                    spEdit.putString(BLUETOOTH_ADDRESS, selectedDevice.getAddr());
//                    spEdit.apply();
                    sp.putString(BluetoothPreferences.Key.BT_ADDR, selectedDevice.getAddr())
                            .putString(BluetoothPreferences.Key.BT_NAME, selectedDevice.getName())
                            .commit();
                    reloadText();
                    // connect
//                    connect();
                }
                break;
            }
        }
    }

    public void connect() {
        bPrintNow.setEnabled(false);
        String addr = sp.getString(BluetoothPreferences.Key.BT_ADDR);
        if (addr != null) {
            if (BluetoothAdapter.checkBluetoothAddress(addr)) {
                BluetoothDevice dev = btAdapter.getRemoteDevice(addr);
                btService.connect(dev);
            } else {
                Toast.makeText(this, "Alamat bluetooth tidak valid", Toast.LENGTH_SHORT).show();
                bPrintNow.setEnabled(true);
            }
        } else {
            Toast.makeText(this, "Belum ada perangkat terpilih", Toast.LENGTH_SHORT).show();
            showScanActivity();
        }
    }

    public void print() {
        if (getIntent().getIntExtra(PRINT_TYPE, 0) == PRINT_DIRECT && dataPrint != null) {
            btService.write(dataPrint);
            btService.write(Command.LF);
        } else if (getIntent().getIntExtra(PRINT_TYPE, 0) == PRINT_STATIC && BluetoothPrint.staticPrintable != null) {
            btService.write(BluetoothPrint.staticPrintable.getPrint(BluetoothPrint.Size.WIDTH80.len,BluetoothPrint.Size.WIDTH80.width));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (btService != null) {
            if (btService.getState() == STATE_NONE) {
                if (btAdapter.isEnabled()) {
                    btService.start();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (btService != null) {
            btService.stop();
        }
        BluetoothPrint.staticPrintable = null;
    }


    @Override
    public void finish() {
        super.finish();
    }
}