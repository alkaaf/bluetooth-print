package org.alkaaf.btprint;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class ActivityUSB extends ActivityBase {
    public static final String INTENT_DATA_PRINT = "data.print.usb";
    private UsbManager mUsbManager;
    private UsbDevice mDevice;
    private PendingIntent mPermissionIntent;
    UsbDeviceConnection connection;
    String TAG = "USB";
    private static final String ACTION_USB_PERMISSION = "com.pradeep.usbprinter.USB_PERMISSION";
    IntentFilter filter;

    ArrayList<UsbDevice> listDevice = new ArrayList<UsbDevice>();
    ArrayAdapter<UsbDevice> listDeviceAdapter;
    BluetoothPrint.Builder builder = new BluetoothPrint.Builder(BluetoothPrint.Size.WIDTH80);
    byte[] text;

    ListView lv;
    Button bRefresh;
    TextView tvInfo;
    TextView tvInfoPrint;

    int buff = 4096;
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    mDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (mDevice != null) {
                            printMessage(ActivityUSB.this, text, mDevice);
                        }
                    } else {
                        Log.d(TAG, "permission denied for device " + mDevice);
                    }
                }
            }
            if(UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action) || UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)){
                refresh();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usb_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Print via USB");
        }
        text = getIntent().getByteArrayExtra(INTENT_DATA_PRINT);
        if(text == null){
            Toast.makeText(this, "Data kosong", Toast.LENGTH_SHORT).show();
            finish();
        }

        lv = findViewById(R.id.lv);
        bRefresh = findViewById(R.id.bRefresh);
        tvInfo = findViewById(R.id.tvInfo);
        tvInfoPrint = findViewById(R.id.tvPrintInfo);

        listDeviceAdapter = new ArrayAdapter<UsbDevice>(this, android.R.layout.simple_list_item_1, listDevice) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = LayoutInflater.from(getContext()).inflate(R.layout.row_usb, parent, false);
                TextView tv1 = v.findViewById(R.id.tv1);
                TextView tv2 = v.findViewById(R.id.tv2);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    tv1.setText(getItem(position).getProductName());
                }
                tv2.setText(getItem(position).getDeviceName());
                return v;
            }
        };
        lv.setAdapter(listDeviceAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                printMessage(ActivityUSB.this, text, listDevice.get(position));
            }
        });
        bRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });


        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        final String ACTION_USB_PERMISSION = "com.pradeep.usbprinter.USB_PERMISSION";
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);

        refresh();
    }

    private void refresh() {
        Map<String, UsbDevice> mapDev = mUsbManager.getDeviceList();
        listDevice.clear();
        listDevice.addAll(mapDev.values());
        listDeviceAdapter.notifyDataSetChanged();

        tvInfo.setVisibility(!listDevice.isEmpty() ? View.GONE : View.VISIBLE);
        tvInfoPrint.setVisibility(listDevice.isEmpty() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(mUsbReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mUsbReceiver);
    }

    public void printMessage(Context context, final byte[] msg, UsbDevice mDevice) {
        // TODO Auto-generated method stub
//        final String printdata = msg;

        int devIndex = listDevice.indexOf(mDevice);
        final UsbEndpoint mEndpointBulkOut;
        if (mUsbManager.hasPermission(mDevice)) {
            final UsbInterface intf = mDevice.getInterface(0);
            for (int i = 0; i < intf.getEndpointCount(); i++) {
                UsbEndpoint ep = intf.getEndpoint(i);
                if (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                    if (ep.getDirection() == UsbConstants.USB_DIR_OUT) {
                        mEndpointBulkOut = ep;
                        connection = mUsbManager.openDevice(mDevice);
                        if (connection != null) {
                            Log.e("Connection:", " connected");
                        }
                        connection.claimInterface(intf, true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                int start = 0;
                                int count = buff < msg.length ? buff : msg.length;
                                while(start < msg.length) {
                                    int b = connection.bulkTransfer(mEndpointBulkOut, msg, start, count, 0);
                                    start = start+b;
                                    count  = buff < msg.length-start ? buff : msg.length-start;
                                    connection.releaseInterface(intf);
                                }
                            }
                        }).start();
                        break;
                    }
                }
            }
        } else {
            mUsbManager.requestPermission(mDevice, mPermissionIntent);
        }
    }

    public void closeConnection(Context context) {
        BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        Toast.makeText(context, "Device closed", Toast.LENGTH_SHORT).show();
                        connection.close();
                    }
                }
            }
        };
    }
}
